class Report < ActiveRecord::Base

  belongs_to :shipment
  belongs_to :sale
  belongs_to :product

  Report.delete_all

  def total_gross_revenue
    total_count = 0
    Sale.all.each do |r|
      total_count += r.saleRevenue
    end
    total_count
  end

  def week_avg_sold
    total_count = 0
    Sale.all.each do |s|
      total_count += s.quantity
    end
    total_count/52
end

  def total_sold
    total_count = 0
    Sale.all.each do |s|
      total_count += s.quantity
    end
    total_count
  end




  end




