class Sale < ActiveRecord::Base

  belongs_to :product



validates_presence_of :product_id, :quantity
validates :quantity, numericality: {greater_than_or_equal_to: 0}

  end