class Product < ActiveRecord::Base

  has_many :shipments
  has_many :sales

  validates_presence_of :category, :price
  validates :price, numericality: {greater_than_or_equal_to: 0}

  validates_presence_of :category, :quantityInStock
  validates :quantityInStock, numericality: {greater_than_or_equal_to: 0}

  def total_count
    total_count = 0
    Shipment.all.each do |c|
      total_count += c.quantity
    end
    total_count
  end

  def total_sold
    total_sold = 0
    Sale.all.each do |c|
      total_sold += c.quantity
    end
    total_sold
  end

  def quantityInStock
  (stock_all - total_sold)
  end



  def stock_all
    stock_all = 0
    Shipment.all.each do |c|
      stock_all += c.quantity
    end
    stock_all
  end


  def low
    if quantityInStock < 5
      "Restock"
    else "Enough in stock"
    end
  end




  def sum_sold
    Shipment.sum( :quantity)
  end

  def sum_order
    Shipment.sum(:quantity)
  end
end