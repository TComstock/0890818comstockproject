# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  $("#sale_product_id").change ->
    val = @value
    url = '/products/' + val + '.json'
    request = $.getJSON url
    request.success (data) ->
      quantity = parseFloat($("#sale_quantity").val())
      $("#sale_saleRevenue").val(quantity * parseFloat(data.price))