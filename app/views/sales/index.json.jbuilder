json.array!(@sales) do |sale|
  json.extract! sale, :id, :product_id, :quantity, :saleRevenue, :dateOut
  json.url sale_url(sale, format: :json)
end
