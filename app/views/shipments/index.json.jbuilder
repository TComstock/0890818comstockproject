json.array!(@shipments) do |shipment|
  json.extract! shipment, :id, :product_id, :quantity, :DateIn
  json.url shipment_url(shipment, format: :json)
end
