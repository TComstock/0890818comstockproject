json.array!(@products) do |product|
  json.extract! product, :id, :category, :price, :quantityInStock
  json.url product_url(product, format: :json)
end
