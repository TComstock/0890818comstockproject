# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Products = Product.create([ {category: 'Shirt', price: '25.00', quantityInStock:'28'},
                            {category: 'Shoes', price: '65.00', quantityInStock:'10'},
                            {category: 'Pants', price: '25.00', quantityInStock:'19'}


                          ])

