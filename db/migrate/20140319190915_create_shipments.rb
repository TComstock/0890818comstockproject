class CreateShipments < ActiveRecord::Migration
  def change
    create_table :shipments do |t|
      t.integer :product_id
      t.integer :quantity
      t.date :DateIn

      t.timestamps
    end
  end
end
