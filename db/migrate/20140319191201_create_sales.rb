class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :product_id
      t.integer :quantity
      t.decimal :saleRevenue
      t.date :dateOut

      t.timestamps
    end
  end
end
