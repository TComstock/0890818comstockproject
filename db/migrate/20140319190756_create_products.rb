class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :category
      t.decimal :price
      t.integer :quantityInStock

      t.timestamps
    end
  end
end
